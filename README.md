# Projeto Desafio Sicred - Funcional Teste Web

### 🛠️ Ferramentas

 - Java 8
 - Maven 3.8.1
 - TestNg 6.14.3
 - Selenium WebDriver 3.141.59 
 - Log4j 1.2.17
 
### Configuração do projeto
- Realizar o import do projeto na IDE desejada;
- Na raiz do projeto mvn clean, mvn install;

### Execução do projeto:
- Acessar ${basedir}/src/main/resources/suites/suite.xml executar o teste pela a IDE;
