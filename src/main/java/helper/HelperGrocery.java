package helper;

import org.openqa.selenium.Keys;

import pageObject.PageGrocery;
import support.Sicredi;

public class HelperGrocery extends Sicredi{
	
	private PageGrocery page;
	
	public HelperGrocery() {
		page = new PageGrocery();
	}
	
	public void changingVersion() {
		click(page.selectVersion);
		click(page.selectionVersion);
	}
	
	public void selectAddCustomer() {
		awaitsElement(page.btn_addCustomer);
		click(page.btn_addCustomer);
	}
	
	public void fillName(String name) {
		awaitsElement(page.name);
		click(page.name);
		fillField(page.name, name);
	}
	
	public void fillLastName(String lastName) {
		fillField(page.contactLastName, lastName);
	}
	
	public void fillContactFirstName(String firstName) {
		fillField(page.contactFirstName, firstName);
	}
	
	public void fillPhone(String phone) {
		fillField(page.phone, phone);
	}
	
	public void fillAddressLine1(String AddressLine1) {
		fillField(page.AddressLine1 , AddressLine1);
	}
	
	public void fillAddressLine2(String AddressLine2) {
		fillField(page.addressLine2, AddressLine2);
	}
	
	public void fillCity(String city) {
		fillField(page.city, city);
	}
	
	public void fillState(String state) {
		fillField(page.state, state);
	}
	
	public void fillPostalCode(String postalCode) {
		fillField(page.postalCode, postalCode);
	}
	
	public void fillContry(String country) {
		fillField(page.country, country);
	}
	
	public void fillFromEnloyeer(String employeer) {
		awaitsElement(page.employeer);
		click(page.employeer);
		fillField(page.fillEmployeer, employeer);
		actionsKeyboard(Keys.ENTER);
	}
	
	public void fillCreditLimit(String creditLimit) {
		fillField(page.creditLimit, creditLimit);
	}
	
	public void selectSave() {
		click(page.save);
	}
	
	public void checkMessageSuccess(String text) {
		checkpoint(page.checkMessage, text);
	}
	
	public void selectGoBackToList() {
		click(page.goBackToList);
	}
	
	public void fillSearchName(String name) {
		fillField(page.searchName, name);
		actionsKeyboard(Keys.ENTER);
	}
	
	public void selectCheckbox () {
		click(page.checkbox);
	}
	
	public void selectDelete() {
		click(page.delete);
	}
	
	public void checkMessageDelete(String text) {
		checkpoint(page.messageDelete, text);
	}
	
	public void selectButtonDelete() {
		click(page.buttonDelete);
	}
	
	public void checkMessageDeleteSucess(String text) {
		checkpoint(page.mensageSucess, text);
	}
	
}
