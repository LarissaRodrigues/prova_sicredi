package support;

import static org.testng.Assert.assertEquals;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import uteis.Log;

public class Sicredi {

	public static WebDriver driver;
	public static WebDriverWait wait;

	public void accessApplication() {
		getDriver().get("https://www.grocerycrud.com/demo/bootstrap_theme");
		getDriver().manage().window().maximize();
	}

	public void actionsKeyboard(Keys key) {
		Log.info("Interagindo com o teclado ");
		new Actions(getDriver()).sendKeys(key).build().perform();
	}

	public void click(WebElement elemento) {
		Log.info("Realizando clique no elemento: " + elemento);
		awaitsElement(ExpectedConditions.elementToBeClickable(elemento)).click();
	}

	public void fillField(WebElement elemento, String valor) {
		Log.info("Preenchendo campo: com o valor: " + valor);
		awaitsElement(ExpectedConditions.elementToBeClickable(elemento)).sendKeys(valor);
	}

	public WebElement awaitsElement(WebElement elemento) {
		Log.info("Aguardando elemento: " + elemento);
		return awaitsElement(ExpectedConditions.refreshed(ExpectedConditions.visibilityOf(elemento)));
	}

	private WebElement awaitsElement(ExpectedCondition<WebElement> expect) {
		return wait.until(ExpectedConditions.refreshed(expect));
	}

	public void checkpoint(WebElement elemento, String texto) {
		awaitsElement(elemento);
		assertEquals(elemento.getText(), texto);
	}

	public static WebDriver getDriver() {
		return Sicredi.driver;
	}

	public static void setDriver(WebDriver driver) {
		Sicredi.driver = driver;
	}

	public static void setWait(WebDriver driver) {
		Sicredi.wait = new WebDriverWait(getDriver(), 30);
	}
}