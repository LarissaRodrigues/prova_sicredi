package test;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import driver.Driver;
import helper.HelperGrocery;

public class GroceryTest extends Driver {

	private HelperGrocery helper;

	@BeforeClass
	public void init() {
		helper = new HelperGrocery();
	}

	@Test
	public void CEN01_Register() {
		helper.accessApplication();
		helper.changingVersion();
		helper.selectAddCustomer();
		helper.fillName("Teste Sicredi");
		helper.fillLastName("Teste");
		helper.fillContactFirstName("seu nome");
		helper.fillPhone("51 9999-9999");
		helper.fillAddressLine1("Av Assis Brasil, 3970");
		helper.fillAddressLine2("Torre D");
		helper.fillCity("Porto Alegre");
		helper.fillState("RS");
		helper.fillPostalCode("91000-000");
		helper.fillContry("Brasil");
		helper.fillFromEnloyeer("Fixter");
		helper.fillCreditLimit("200");
		helper.selectSave();
		helper.checkMessageSuccess(
				"Your data has been successfully stored into the database. Edit Customer or Go back to list");
	}

	@Test
	public void CEN02_Delete() {
		helper.selectGoBackToList();
		helper.fillSearchName("Teste Sicredi");
		helper.selectCheckbox();
		helper.selectDelete();
		helper.checkMessageDelete("Are you sure that you want to delete this 1 item?");
		helper.selectButtonDelete();
		helper.checkMessageDeleteSucess("Your data has been successfully deleted from the database.");
	}

}
