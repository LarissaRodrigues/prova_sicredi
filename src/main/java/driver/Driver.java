package driver;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import support.Sicredi;
import uteis.Log;

public class Driver {
	public WebDriver driver;

	@BeforeTest
	public void getDriver() {
		Log.info("Carregando as configurações Chrome");
		if (null == driver) {
			File arquivo = new File("src/main/resources/drivers/chromedriver.exe");
			System.setProperty("webdriver.chrome.driver", arquivo.getAbsolutePath());
			driver = new ChromeDriver();
			setDependencies();
		}
	}

	@AfterTest(alwaysRun = true)
	public void quit() {
		driver.close();
		driver.quit();
	}

	public void setDependencies() {
		Sicredi.setDriver(driver);
		Sicredi.setWait(driver);
	}
}