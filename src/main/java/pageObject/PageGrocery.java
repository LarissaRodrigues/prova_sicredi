package pageObject;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import support.Sicredi;

public class PageGrocery {

	public PageGrocery() {
		PageFactory.initElements(Sicredi.getDriver(), this);
	}

	@FindBy(id = "switch-version-select")
	public WebElement selectVersion;

	@FindBy(xpath = "//option[text()='Bootstrap V4 Theme']")
	public WebElement selectionVersion;

	@FindBy(xpath = "//div[@class='header-tools']//a[@class='btn btn-default btn-outline-dark']")
	public WebElement btn_addCustomer;

	@FindBy(id = "field-customerName")
	public WebElement name;

	@FindBy(id = "field-contactLastName")
	public WebElement contactLastName;

	@FindBy(id = "field-contactFirstName")
	public WebElement contactFirstName;

	@FindBy(id = "field-phone")
	public WebElement phone;

	@FindBy(id = "field-addressLine1")
	public WebElement AddressLine1;

	@FindBy(id = "field-addressLine2")
	public WebElement addressLine2;

	@FindBy(id = "field-city")
	public WebElement city;

	@FindBy(id = "field-state")
	public WebElement state;

	@FindBy(id = "field-postalCode")
	public WebElement postalCode;

	@FindBy(id = "field-country")
	public WebElement country;

	@FindBy(xpath = "//a[@class='chosen-single chosen-default']")
	public WebElement employeer;

	@FindBy(xpath = "//div[@class='chosen-drop']//input")
	public WebElement fillEmployeer;

	@FindBy(xpath = "//li[@class='active-result result-selected']")
	public WebElement selecionaEmpregador;

	@FindBy(id = "field-creditLimit")
	public WebElement creditLimit;

	@FindBy(id = "form-button-save")
	public WebElement save;

	@FindBy(xpath = "//div[@class='report-div success bg-success']//p")
	public WebElement checkMessage;

	@FindBy(linkText = "Go back to list")
	public WebElement goBackToList;

	@FindBy(name = "customerName")
	public WebElement searchName;

	@FindBy(xpath = "//input[@class='select-all-none']")
	public WebElement checkbox;

	@FindBy(xpath = "//span[@class='text-danger']")
	public WebElement delete;

	@FindBy(xpath = "//p[@class = 'alert-delete-multiple-one']")
	public WebElement messageDelete;

	@FindBy(xpath = "//button[@class='btn btn-danger delete-multiple-confirmation-button']")
	public WebElement buttonDelete;

	@FindBy(xpath = "//p[text()='Your data has been successfully deleted from the database.']")
	public WebElement mensageSucess;
}